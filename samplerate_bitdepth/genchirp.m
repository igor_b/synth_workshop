function y=genchirp()
  t = 0:1/48000.0:4;
  y=chirp(t, 0, 5, 16000);
