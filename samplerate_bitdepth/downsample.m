

function [ww, ffs] = downsample(w, fs, div, play=1)
  ww = w(1:div:end);
  ww = ww./max(ww);
  ffs = fs/div;
  disp(['Sample rate: ' num2str(ffs) 'Hz']);
  if play
    sound(ww, ffs);
  endif
endfunction
