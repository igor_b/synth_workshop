function spec(w, fs, fdiv=2)
  f = abs(fft(w, 4096));
  x = linspace(0, fs/fdiv, length(f)/fdiv);
  plot(x, f(1:length(f)/fdiv));
  axis([0 fs/fdiv 0 max(f)]);
  xlabel('Hz');
  grid;
