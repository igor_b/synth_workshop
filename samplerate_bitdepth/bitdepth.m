

function [ww, ffs] = bitdepth(w, fs, nbit, play=1)
  sm = 2^(nbit-1)-1;
  ww = floor(w.*sm)./sm;
  ffs = fs;
  if play
    sound(ww, fs);
  endif
endfunction
