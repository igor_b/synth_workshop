import processing.sound.*;


final int samplerate = 44100;
final float duration = 2.0;
final int sample_len = int(duration*samplerate);
float[] signal = new float[sample_len];
AudioSample sample = new AudioSample(this, signal);


void play(float freq) {
  final float T = 1.0/freq;
  int cnt = 0;
  for(int i = 0; i < sample_len; i++){
    // sine
    signal[i] = sin(TWO_PI*i/samplerate*freq);
    
    // noise
    //signal[i] = random(255)/255.0*2-1;

    // square
    //if(cnt/(float)samplerate<T/2) signal[i] = 0.7;
    //else signal[i] = -0.7;
    //cnt++;
    //if(cnt/(float)samplerate>=T) cnt = 0;
  }

  // play
  sample.write(signal);
  sample.cue(0);
  sample.play();
}


class KButton {
  private int x;
  private int y;
  private int w;
  private int h;
  private String name;
  private Boolean clicked = false;
  private Boolean pressedold = false;
  private int triggered = 0;
  public KButton(int x, int y, int w, int h, String name) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.name = name;
  }

  private Boolean mouseover() {
    return (mouseX>this.x-this.w && mouseX<this.x+this.w && mouseY>this.y-this.h && mouseY<this.y+this.h);
  }
  public Boolean is_clicked() { 
    return this.clicked;
  }
  public String getname() { 
    return this.name;
  }
  public void trigger() { 
    this.triggered = 5;
  }

  public void redraw(String suffix) {
    Boolean pressed = this.mouseover() && mousePressed;
    stroke(255);
    if (this.mouseover()) {
      if (pressed) {
        fill(200);
      } else {
        fill(150);
      }
    } else {
      fill(90);
    }
    if (this.triggered>0) {
      fill(200);
    }
    rect(this.x, this.y, this.w, this.h);
    fill(0);
    text(this.name+suffix, this.x-textWidth(this.name+suffix)/2, this.y+text_size/2);

    this.clicked = (pressed && !this.pressedold) || (this.triggered==5);
    this.pressedold = pressed;
    if (this.triggered>0) this.triggered--;
  }
}

public static final int numkeys = 8;
int octave = 4;


final int text_size = 12;

KButton keys[] = new KButton[numkeys];

final String[] notenames = {"C", "D", "E", "F", "G", "A", "B", "C"};
final float[] chromatic = {16.351, 17.324, 18.354, 19.445, 20.601, 21.827, 23.124, 24.499, 25.956, 27.50, 29.135, 30.868, 32.703};

final int[] scale = {0, 2, 4, 5, 7, 9, 11, 12};    // major
//final int[] scale = {0, 2, 3, 5, 7, 8, 10, 12};  // minor

float[] notefreqs = new float[numkeys];

void setup() {
  size(640, 360);
  textSize(text_size);

  rectMode(RADIUS);

  for(int i=0; i<numkeys; i++) {
    // create scale
    notefreqs[i] = chromatic[scale[i]];

    // create "piano" keys
    keys[i] = new KButton(width/(numkeys)/2+width/(numkeys)*(i), 250, int(width/numkeys/2*0.7), 70, notenames[i]);
  }

}

void draw() { 
  background(0);

  for (int i=0; i<numkeys; i++) {
    keys[i].redraw(String.valueOf(octave));
    if (keys[i].clicked) {
      println(String.format("click %s (%.2f)", keys[i].name, notefreqs[i]*Math.pow(2, octave)));
      play(notefreqs[i]*(float)Math.pow(2, octave));
    }
  }
}

void keyPressed() {
  if (key >= '1' && key <= '8') {
    keys[key-'1'].trigger();
  } 
  else{
    switch(key){
      case '+':
        octave++;
        break;
      case '-':
        octave--;
        break;
    }
  }
}
