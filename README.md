# Digital Sound Synthesis

workshop by Igor Brkic, organized by Radiona.org makerspace on 2nd and 3rd of May 2020.

### Content:
- `samplerate_bandwidth` - octave/matlab scripts for simple downsample/bit depth demonstration
- `bandlimited_waveforms.ipynb` - Jupyter notebook containing code for bandlimited waveforms and envelope tables
- `synth_workshop_01_basic` - Processing sketch with basic GUI and sound
- `synth_workshop_02_wavetable` - Processing sketch with sound generation using wave tables
- `synth_workshop_03_envelope` - Processing sketch with envelope generator
- `synth_workshop_04_filter` - Processing sketch with additional oscillator and simple resonant filter

